"use strict";

// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph"
//  і додайте його до розділу <section id="content">
const btn = document.getElementById("btn-click");
const section = document.getElementById("content");

btn.addEventListener("click", function () {
  const p = document.createElement("p");
  p.textContent = "New Paragraph";
  section.appendChild(p);
});
//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//   По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder,
//  і name. та додайте його під кнопкою.

const btnCreateInput = document.createElement("button");
btnCreateInput.textContent = "Створити input";
btnCreateInput.setAttribute("id", "btn-input-create");
section.parentNode.insertBefore(btnCreateInput, section.nextSibling);

btnCreateInput.addEventListener("click", function () {
  const newInput = document.createElement("input");
  newInput.setAttribute("type", "text");
  newInput.setAttribute("placeholder", "Введіть текст");
  newInput.setAttribute("name", "inputName");
  section.appendChild(newInput);
});
